#include "PointCloudInterface.h"

void PointCloudInterface::addGenerator(PointCloudGenerator* generator)
{
	generators.push_back(generator);
}

void PointCloudInterface::setRecorder(PointCloudRecorder* recorder)
{
	this->recorder = recorder;
}

bool PointCloudInterface::generate()
{
	for (int i = 0; i < generators.size(); i++)
	{
		PointCloud generated = generators[i]->captureFor();
		pointCloud = pointCloud + generated;
	}
	return true;
}

bool PointCloudInterface::record()
{
	if (recorder->save(pointCloud)) return true;
	return false;
}
