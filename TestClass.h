/**
* @file PointCloud.h
* @author Ahmet Ata �ent�rk (152120181120)
* @date 17.01.2022
* @brief Header of PointCloud class.
*
*	This file is a header of the TestClass and there is one static function to do test.
*/

#pragma once
#include <iostream>
#include "Transform.h"
#include "Point.h"
#include "PointCloud.h"
#include "PointCloudRecorder.h"
#include "DepthCamera.h"
#include "RadiusOutlierFilter.h"
#include "PassThroughFilter.h"
#include "PointCloudFilter.h"
#include "FilterPipe.h"
#include "PointCloudGenerator.h"
#include "PointCloudInterface.h"

//!  Test class does every test of interface of the project
/*!
* It includes last and completed test of the project
*/
static class TestClass
{
public:
	static void doTest();
};

