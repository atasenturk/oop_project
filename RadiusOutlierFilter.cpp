/**
* @file RadiusOutlierFilter.h
* @author Ol�an Sat�r (152120171109)
* @date 13.01.2022
* @brief Source file of RadiusOutlierFilter class.
*
*This file includes all the implementations declared in the RadiusOutlierFilter header file.
*/

#include "RadiusOutlierFilter.h"

void RadiusOutlierFilter::setRadius(double radius)
{
	this->radius = radius;
}

double RadiusOutlierFilter::getRadius()
{
	return radius;
}

void RadiusOutlierFilter::filter(PointCloud& pc)
{
	list<Point> points = pc.getAllPoints();
	list<Point>::iterator it;
	list<Point>::iterator it2;
	PointCloud filtered;
	bool admit = false;
	int loop = points.size();
	for (it = points.begin(); it != points.end(); ++it)
	{
		for (it2 = points.begin(); it2 != points.end(); ++it2)
		{
			if (it != it2) {
				if (it->distance(*it2) <= radius) {
					admit = true;
				}
			}
		}
		if (admit == true) {
			filtered.addElementToEnd(*it);
		}
		admit = false;
	}
	pc = filtered;
}