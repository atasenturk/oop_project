/**
* @file PointCloudFilter.h
* @author Ol�an Sat�r (152120171109)
* @date 15.01.2022
* @brief Header of PointCloudFilter class.
*
* This file is used to implement depth camera functions.
*/

#pragma once
#include "PointCloud.h"
class PointCloudFilter
{

public:

    /**
	* This function acts as an abstract class for RadiusOutlier and PassThrought. These two filter classes inherit from this and must write the content of this function.
	* @param pc: Retrieves the point cloud value to be filtered.
	*/
	virtual void filter(PointCloud& pc) = 0; //This function is a pure virtual function.
};

