/**
* @file FilterPipe.cpp
* @author Ol�an Sat�r (152120171109)
* @date 15.01.2022
* @brief Source file of FilterPipe class.
*
* This file includes all the implementations declared in the FilterPipe header file.
*/

#include "FilterPipe.h"

FilterPipe::FilterPipe()
{
}

void FilterPipe::addFilter(PointCloudFilter* filter)
{
	filters.push_back(filter);
}

void FilterPipe::filterOut(PointCloud& pc)
{
	for (int i = 0; i < filters.size(); i++)
	{
		filters[i]->filter(pc);
	}
}
