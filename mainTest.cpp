/***
* @file mainTest.cpp
* @author K�smet AKTA� (152120191088)
* @date 17.01.2022
* @brief Test file of PointCloudInterface class.
*
***This file was written for testing purposes.
***/


#include <iostream>
#include "TestClass.h"

using namespace std;

int main()
{
	//This function tests operations in TestClass.
	TestClass::doTest();
}