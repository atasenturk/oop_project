/**
* @file Point.cpp
* @author Ol�an Sat�r (152120171109)
* @date 20.12.2021
* @brief Source file of point class.
*
* This file includes all the implementations declared in the Point header file.
*/


#include "Point.h""

double Point::getX()
{
	return x;
}

double Point::getY()
{
	return y;
}

double Point::getZ()
{
	return z;
}

void Point::setX(double x)
{
	this->x = x;
}

void Point::setY(double y)
{
	this->y = y;
}

void Point::setZ(double z)
{
	this->z = z;
}

bool Point::operator==(const Point& point)
{
	return (x == point.x && y == point.y && z == point.z) ? true : false;
}

double Point::distance(const Point& point) const
{
	return sqrt(pow((x - point.x), 2) + pow((y - point.y), 2) + pow((z - point.z), 2));
}
