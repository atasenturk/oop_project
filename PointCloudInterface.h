/**
* @file PointCloudInterface.h
* @author K�smet AKTA� (152120191088)
* @date 17.01.2022
* @brief Header of class PointCloudInterface class.
*
* This file includes all the declarations of member variables and functions for class PointCloudInterface class.
*/


#pragma once
#include "PointCloud.h"
#include "PointCloudGenerator.h"
#include "PointCloudRecorder.h"
#include <vector>

//!  This class is a class used to simplify operations. It has two member functions. 
/*!
*	When the generate() function is called, the captureFor function is called from
*	all the objects in the generators member, providing point clouds. Each point cloud
*	is then added to the pointCloud member.
*/
class PointCloudInterface
{
private:
	PointCloud pointCloud;
	PointCloud patch;
	vector<PointCloudGenerator*> generators;
	PointCloudRecorder* recorder;
public:

	/** Constructor for PointCloudInterface class.
	*/
	PointCloudInterface(){}

	/** This function adds new generator to the pointer array.
	* @param generator is the new generator to add
	*/
	void addGenerator(PointCloudGenerator* generator);

	/** This function sets the recorder.
	* @param recorder is the recorder that is wanted to be change
	*/
	void setRecorder(PointCloudRecorder* recorder);

	/** This function obtains point clouds by calling the captureFor function from all objects in the generators member.
	*/
	bool generate();

	/** This function saves points in pointCloud to file.
	*/
	bool record();
};

