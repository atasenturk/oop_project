/**
* @file TestClass.cpp
* @author Ahmet Ata �ent�rk (152120181120)
* @date 17.01.2022
* @brief Source file of the TestClass
*
*	This file is a source file of the TestClass and there is one static function to do test.
*/

#include "TestClass.h"
#include <list>
#include "Eigen/Dense"

void TestClass::doTest()
{
	PointCloudRecorder pointCloudRecorder("output.txt");

	//FilterPipe object for first camera
	FilterPipe filterPipeFirstCamera;
	filterPipeFirstCamera.addFilter(new RadiusOutlierFilter(25));
	filterPipeFirstCamera.addFilter(new PassThroughFilter(400, 0, 400, 0, 45, -45));


	//FilterPipe object for second camera
	FilterPipe filterPipeSecondCamera;
	filterPipeSecondCamera.addFilter(new RadiusOutlierFilter(25));
	filterPipeSecondCamera.addFilter(new PassThroughFilter(500, 0, 500, 0, 45, -45));

	//Camera translations and rotations arrays
	Eigen::Vector3d firstCameraTr;
	Eigen::Vector3d secondCameraTr;
	firstCameraTr << 100, 500, 50;
	secondCameraTr << 550, 150, 50;

	Eigen::Vector3d firstCameraRot;
	Eigen::Vector3d secondCameraRot;
	firstCameraRot << 0, 0, -90;
	secondCameraRot << 0, 0, 90;

	//Transform object for first and second camera
	Transform transformForFirstCamera(firstCameraTr, firstCameraRot);
	Transform transformForSecondCamera(secondCameraTr, secondCameraRot);

	//PointCloudGenerator object for first and second camera and text files of cameras
	PointCloudGenerator* generatorForFirstCamera = new DepthCamera("camera1.txt", transformForFirstCamera, &filterPipeFirstCamera);
	PointCloudGenerator* generatorForSecondCamera = new DepthCamera("camera2.txt", transformForSecondCamera, &filterPipeSecondCamera);

	//PointCloudInterface object declaration
	PointCloudInterface pointCloudInterface;

	//Setting PointCloudRecorder in PointCloudInterface
	pointCloudInterface.setRecorder(&pointCloudRecorder);

	//Adding generators to the interface
	pointCloudInterface.addGenerator(generatorForFirstCamera);
	pointCloudInterface.addGenerator(generatorForSecondCamera);

	//Generate method for creating pointclouds which are filtered by two filter and also transformed
	pointCloudInterface.generate();

	//Recording the PointCloud to the text file as output
	pointCloudInterface.record();

}