#include "PointCloudRecorder.h"
#include "PointCloud.h"
#include "DepthCamera.h"
#include "PointCloudGenerator.h"
#include "RadiusOutlierFilter.h"
#include "PassThroughFilter.h"

int main()
{

	FilterPipe filterPipeFirstCamera;
	filterPipeFirstCamera.addFilter(new RadiusOutlierFilter(25));
	filterPipeFirstCamera.addFilter(new PassThroughFilter(400, 0, 400, 0, 45, -45));

	FilterPipe filterPipeSecondCamera;
	filterPipeSecondCamera.addFilter(new RadiusOutlierFilter(25));
	filterPipeSecondCamera.addFilter(new PassThroughFilter(500, 0, 500, 0, 45, -45));


	Eigen::Vector3d firstCameraTr;
	Eigen::Vector3d secondCameraTr;
	firstCameraTr << 100, 500, 50;
	secondCameraTr << 550, 150, 50;

	Eigen::Vector3d firstCameraRot;
	Eigen::Vector3d secondCameraRot;
	firstCameraRot << 0, 0, -90;
	secondCameraRot << 0, 0, 90;
	Transform transformForFirstCamera(firstCameraTr, firstCameraRot);
	Transform transformForSecondCamera(secondCameraTr, secondCameraRot);

	DepthCamera depthCamera("camera1.txt", transformForFirstCamera, &filterPipeFirstCamera);
	PointCloud pointCloud;
	pointCloud = depthCamera.capture();
	pointCloud.printPoints();
}